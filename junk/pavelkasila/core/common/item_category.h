//
// Created by Pavel Kasila on 8.05.24.
//

#ifndef CREATIVE_ITEM_CATEGORY_H
#define CREATIVE_ITEM_CATEGORY_H

#include "junk/pavelkasila/core/database/abstract_entity.h"

#include <QSqlRecord>
#include <QString>

namespace outfit::core::common {
class ItemCategory : public database::AbstractEntity<int> {
   public:
    ItemCategory() = default;

    explicit ItemCategory(const QSqlRecord& record);

    QString getName();

    void setName(const QString& name);

   private:
    QString name_;
};
}  // namespace outfit::core::common

#endif  // CREATIVE_ITEM_CATEGORY_H
