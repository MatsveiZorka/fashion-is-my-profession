#include "junk/pavelkasila/core/database/database_manager.h"
#include "junk/pavelkasila/core/statistics/statistics_view.h"

#include <QLabel>
#include <QMessageBox>
#include <QThread>
#include <QtWidgets/QApplication>
#include <memory>

int main(int argc, char** argv) {
    const QApplication app(argc, argv);

    if (!outfit::core::database::DatabaseManager::getInstance().connect()) {
        QMessageBox msg;
        msg.setText("not opened");
        msg.exec();
        return 1;
    }

    std::unique_ptr<outfit::core::statistics::StatisticsView> w =
        std::make_unique<outfit::core::statistics::StatisticsView>();
    w->setWindowTitle("Outfit Manager");
    w->resize(400, 600);
    w->show();

    return QApplication::exec();
}