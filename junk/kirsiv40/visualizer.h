#include <QThread>
#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QLabel>
#include <QtWidgets/QStylePainter>
#include <QTextEdit>
#include <QPushButton>
#include <QFileDialog>
#include <QCheckBox>
#include <QRadioButton>
#include <QSlider>

class CleverLabel;

class WorkerThread : public QThread
{
   public:
    std::string path1;
    std::string path2;
    std::string cpos;

    void SetUp(std::string, std::string, std::string);

    void run() override;
};

class PositionChooseButtons : public QWidget {
   public:
    QRadioButton* top;
    QRadioButton* bot;
    QRadioButton* dress;

    PositionChooseButtons(QWidget*);

    void SetUp();
};

class NeuroResult : public QLabel {
    Q_OBJECT
   public:
    void mouseDoubleClickEvent(QMouseEvent*) override;

    NeuroResult(CleverLabel*, QWidget*);

   private:
    CleverLabel* _parent;
};

class CleverLabel : public QWidget {
    Q_OBJECT
   public:
    QLabel* label;
    QLabel* label2;
    WorkerThread* thr;
    QPushButton* but;
    QString fileName;
    QString fileName2;
    NeuroResult* neuroresultimage;
    PositionChooseButtons* cb;
    std::string resultpath;

    int gScale;
    int steps;
    int seed;

    CleverLabel(QWidget*, QPushButton*, PositionChooseButtons*);

    void SetUp();
    void SetUpConnections();
    void ChangeNeuroValues(int, int, int);

   public slots:
    void ShowImage();
    void NeuroHandler();
};

class ImageOpenerButton : public QPushButton {
    Q_OBJECT
   public:
    ImageOpenerButton(QWidget*);
    
    void SetUpConnections();
    void SetUp(QLabel*, QString*);

   public slots:
    void HandlePress();

   private:
    QLabel* _targetimage;
    QString* _targetpath;
};

class ValLabel : public QLabel {
    Q_OBJECT
   public:
    ValLabel(QWidget* parent);

   public slots:
    void SetValue(int);

};

class SettingsScreen : public QWidget {
    Q_OBJECT
   public:
    QSlider* par1;
    QSlider* par2;
    QSlider* par3;

    QLabel* lab1;
    QLabel* lab2;
    QLabel* lab3;

    ValLabel* vallab1;
    ValLabel* vallab2;
    ValLabel* vallab3;

    QPushButton* submit;

    SettingsScreen(QWidget*);
    void SetUp(CleverLabel*);
    void SetUpConnections();

   public slots:
    void Open() {
        show();
    }

    void ChangeValues();

   private:
    CleverLabel* _cl;
};

class Screen : public QWidget {
    Q_OBJECT
   public:
    QPushButton* but;
    QPushButton* settings;
    PositionChooseButtons* ps;

    CleverLabel* cl;

    ImageOpenerButton* firstimagebut;
    ImageOpenerButton* secondimagebut;

    SettingsScreen* settingsScreen;

    void SetUp();
    void SetUpConnections();

};